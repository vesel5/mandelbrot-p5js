// Set the width and height of the canvas
const width = 1000;
const height = 1000;

// Set the initial zoom level
let zoom = 1;

// Set the initial position
let x = 0;
let y = 0;

// Set the start time
let startTime = Date.now();

// Set the zoom interval in milliseconds
const zoomInterval = 1000;

let topLeft = { i: -1, j: -1 };
let topRight = { i: -1, j: -1 };
let bottomLeft = { i: -1, j: -1 };
let bottomRight = { i: -1, j: -1 };

let closestToCenter = { i: -1, j: -1 };
let prevClosestToCenter = { i: -1, j: -1 };
let closestToCenterDistance = Infinity;




function setup() {
    // Create the canvas
    createCanvas(width, height);

    // Set the color mode to HSB
    colorMode(HSB, 1000, 100, 100, 100);

    // Set the background color
    background(0);
}

function draw() {

    // Calculate the width and height of each pixel
    const w = 4 / (width * zoom);
    const h = 4 / (height * zoom);

    // Iterate over every pixel in the canvas
    for (let j = 0; j < height; j++) {
        for (let i = 0; i < width; i++) {
            // Calculate the real and imaginary components of the complex number
            const a = x - 2 + w * i;
            const b = y - 2 + h * j;

            // Set the initial values for the Mandelbrot algorithm
            let x0 = a;
            let y0 = b;
            let x1 = 0;
            let y1 = 0;
            let iteration = 0;
            let maxIterations = 1000;

            // Iterate the Mandelbrot algorithm
            while (x1 * x1 + y1 * y1 < 4 && iteration < maxIterations) {
                const x2 = x1 * x1 - y1 * y1 + x0;
                const y2 = 2 * x1 * y1 + y0;
                x1 = x2;
                y1 = y2;
                iteration++;
            }

            // Set the color of the pixel based on the number of iterations
            const hue = map(iteration, 0, maxIterations, 0, 1000);
            stroke(hue, 100, 100);
            point(i, j);

            if (iteration === maxIterations) {
                // Check if the pixel is the most top left, top right, bottom left, or bottom right pixel
                if (i < topLeft.i || (i === topLeft.i && j < topLeft.j)) {
                    topLeft = { i, j };
                }
                if (i > topRight.i || (i === topRight.i && j < topRight.j)) {
                    topRight = { i, j };
                }
                if (i < bottomLeft.i || (i === bottomLeft.i && j > bottomLeft.j)) {
                    bottomLeft = { i, j };
                }
                if (i > bottomRight.i || (i === bottomRight.i && j > bottomRight.j)) {
                    bottomRight = { i, j };
                }

                let distanceToCenter = Infinity;

                if (prevClosestToCenter.i == -1 && prevClosestToCenter.j == -1) {
                    // Calculate the distance to the center of the canvas for this pixel
                    const centerX = width / 2;
                    const centerY = height / 2;
                    distanceToCenter = Math.sqrt((i - centerX) ** 2 + (j - centerY) ** 2);
                } else {
                    distanceToCenter = Math.sqrt((i - prevClosestToCenter.i) ** 2 + (j - prevClosestToCenter.j) ** 2);
                }

                // Check if this pixel is the closest to the center of the canvas
                if (distanceToCenter < closestToCenterDistance) {
                    closestToCenter = { i, j };
                    closestToCenterDistance = distanceToCenter;
                }


            }

        }
    }

    // Check if it is time to zoom in
    const elapsedTime = Date.now() - startTime;
    if (elapsedTime > zoomInterval) {




        // fill(0, 0, 100);
        // ellipse(topLeft.i, topLeft.j, 150, 300);

        // fill(0, 0, 100);
        // ellipse(topRight.i, topRight.j, 100, 200);

        // fill(0, 0, 100);
        // ellipse(bottomLeft.i, bottomLeft.j, 150, 50);

        // fill(0, 0, 100);
        // ellipse(bottomRight.i, bottomRight.j, 200, 100);

        fill(0, 0, 100);
        ellipse(closestToCenter.i, closestToCenter.j, 100, 100);

        // calculate which of the already calculated points top left, top right, bottom left, bottom right is closest to the center of the screen and zoom in on that point
        const centerX = width / 2;
        const centerY = height / 2;

        let topLeftDistance = dist(centerX, centerY, topLeft.i, topLeft.j);
        if (topLeft.i == -1 && topLeft.j == -1) {
            topLeftDistance = Infinity;
        }


        let topRightDistance = dist(centerX, centerY, topRight.i, topRight.j);
        if (topRight.i == -1 && topRight.j == -1) {
            topRightDistance = Infinity;
        }

        let bottomLeftDistance = dist(centerX, centerY, bottomLeft.i, bottomLeft.j);
        if (bottomLeft.i == -1 && bottomLeft.j == -1) {
            bottomLeftDistance = Infinity;
        }

        let bottomRightDistance = dist(centerX, centerY, bottomRight.i, bottomRight.j);
        if (bottomRight.i == -1 && bottomRight.j == -1) {
            bottomRightDistance = Infinity;
        }


        const minDistance = min(topLeftDistance, topRightDistance, bottomLeftDistance, bottomRightDistance);
        console.log('x: ' + x + ' y: ' + y + ' zoom: ' + zoom + ' minDistance: ' + minDistance + ' topLeftDistance: ' + topLeftDistance + ' topRightDistance: ' + topRightDistance + ' bottomLeftDistance: ' + bottomLeftDistance + ' bottomRightDistance: ' + bottomRightDistance + ' w: ' + w + ' h: ' + h);


        if (closestToCenterDistance != Infinity) {
            x = 2 * (closestToCenter.i - centerX) / centerX
            y = 2 * (closestToCenter.j - centerY) / centerY
        }





        zoom += 0.15;


        // Reset the timer
        startTime = Date.now();

        // Reset the most top left, top right, bottom left, and bottom right pixels
        topLeft.i = -1;
        topLeft.j = -1;


        topRight.i = -1;
        topRight.j = -1;

        bottomLeft.i = -1;
        bottomLeft.j = -1;

        bottomRight.i = -1;
        bottomRight.j = -1;

        // Reset the closest to center pixel
        prevClosestToCenter.i = closestToCenter.i;
        prevClosestToCenter.j = closestToCenter.j;
        closestToCenter.i = -1;
        closestToCenter.j = -1;
        closestToCenterDistance = Infinity;


    }
}





